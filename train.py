from utils.skip_gram import SkipGram
from utils.get_train_data import TrainDataLoader

import torch
from torch.utils.data import DataLoader



def train(dataloader, model):
    optimizer = torch.optim.SGD(model.parameters(), lr=0.2)
    model.train()

    if torch.cuda.is_available():
        print('使用GPU')
        model.cuda()
    else:
        print('使用CUP')

    current = 0
    while True:
        center_lis, pos_lis, neg_lis = dataloader.get_batch()
        optimizer.zero_grad()
        loss = model(center_lis, pos_lis, neg_lis)

        loss.backward()
        optimizer.step()

        if current % 2000 == 0:
            loss = loss.item()
            print('Epoches', dataloader.epoches, 'Step', current, 'Loss', '%.3f'%loss)


        if current % 100000 == 0:
            model.save_wordvector()
        current = current + 1
            




if __name__ == '__main__':
    #配置data_loader
    data_loader = TrainDataLoader()
    data_loader.VOCABULARY_PATH = './data/text8'
    data_loader.DATA_PATH = './data/data.npy'
    data_loader.WORD2IDX_PATH = './data/word2idx.npy'
    data_loader.IDX2WORD_PATH = './data/idx2word.npy'
    data_loader.NEG_FREQUENCY_PATH = './data/frequency.npy'
    data_loader.KEEPRATE_PATH = './data/keeprate.npy'
    
    vocabulary_size = len(data_loader.word2idx.keys())
    print(vocabulary_size)

    #配置模型
    sg = SkipGram(vocabulary_size, SAVE_PATH = './data/')

    train(data_loader, sg)