import numpy as np
import copy

import math
import os
import random

#1. 从词库中读取数据
#2. 使用正负采样生成数据
class TrainDataLoader(object):
    def __init__(self):
        self.VOCABULARY_PATH = './data/text8'
        self.DATA_PATH = './data/data.npy'
        self.WORD2IDX_PATH = './data/word2idx.npy'
        self.IDX2WORD_PATH = './data/idx2word.npy'
        self.NEG_FREQUENCY_PATH = './data/frequency.npy'
        self.KEEPRATE_PATH = './data/keeprate.npy'


        self.COMMON_WORD_THRESHOLD = 1000
        self.RESCALE_FREQ = 0.75

        self.center_word_idx = 0
        self.epoches = 0
        

        #Load Data
        
        
        
        # Load/Generate dictionary and frequency
        try:
            print('从Data文件夹中获取数据')
            self.data = np.load(self.DATA_PATH) # 以下表的形式重写整个此表, 若该词不在词表中, 则下表为1
            self.word2idx = np.load(self.WORD2IDX_PATH, allow_pickle = True).item() # 单词到词表的映射
            self.idx2word = np.load(self.IDX2WORD_PATH, allow_pickle = True).item() # 词表到单词的映射
            self.neg_frequency = np.load(self.NEG_FREQUENCY_PATH, allow_pickle = True)      # 用于负采样的概率
            self.keeprate = np.load(self.KEEPRATE_PATH, allow_pickle = True)            # 用于高频词打压的keep-rate

            print('成功')
        except:  
            print('获取失败')
            print('开始重新生成数据')
            print('读数据集')
            self.data = self.load_data(self.VOCABULARY_PATH)
            print('完成')
            print(' ')
            print('计算频率')
            self.word2idx, self.idx2word, self.neg_frequency, self.keeprate = self.get_dictionary_frequency()
            self.dictionary_keys = self.word2idx.keys()
            print('完成')
            print(' ')
            print('将数据集转换为下标')
            for k in range(len(self.data)):
                if self.data[k] in self.dictionary_keys:
                    self.data[k] = self.word2idx[self.data[k]]
                else:
                    self.data[k] = -1
            self.data = np.array(self.data)
            np.save(self.DATA_PATH ,self.data)
            print('完成')



        self.dictionary_keys = self.word2idx.keys()
        self.idx_lis = np.array(range(len(self.dictionary_keys)))
        self.data_length = len(self.data)


    def load_data(self, DATA_PATH):
        print('Loading data')
        with open(DATA_PATH) as f:
            data = f.read().split()
            data = [x for x in data if x != 'eoood']
        print('Done')
        return data


    def get_dictionary_frequency(self):
        #1. remove the rare words
        #2. storage the others  
        counts = {}
        for word in self.data:
            if word not in counts.keys():
                counts[word] = 1
            else:
                counts[word] = counts[word] + 1


        word2idx = {}
        idx2word = {}
        idx = 0
        tot_word_num = 0

        for word in counts.keys():
            if counts[word] >= self.COMMON_WORD_THRESHOLD:
                word2idx[word] = idx
                idx2word[idx] = word
                idx = idx + 1
                tot_word_num += counts[word]         
            else:
                counts[word] = -1

        np.save(self.WORD2IDX_PATH, word2idx)
        np.save(self.IDX2WORD_PATH, idx2word)

        print(len(counts.keys()))

        neg_frequency = np.zeros(len(idx2word.keys()))
        keep_rate = np.zeros(len(idx2word.keys()))
        for k in idx2word.keys():
            freq = counts[idx2word[k]] / tot_word_num
            print(idx2word[k],counts[idx2word[k]], freq)
            if freq == 0:
                return 1
            neg_frequency[k] = freq ** self.RESCALE_FREQ
            keep_rate[k] = (np.sqrt(freq/0.001) + 1) * 0.001 / freq
        
        keep_rate = np.clip(keep_rate, 0, 1)
        
        #get frequency pre word
        neg_frequency = neg_frequency / np.sum(neg_frequency)
        np.save(self.NEG_FREQUENCY_PATH, neg_frequency)
        np.save(self.KEEPRATE_PATH, keep_rate)

        return word2idx, idx2word, neg_frequency, keep_rate


    def get_batch(self, batch_size = 32, neg_num = 10, window_size = 2):
        '''
        output three numpy arrays
        center_lis: batch_size * 1
        pos_lis:    batch_size * 1
        neg_lis:    neg_num * 1
        '''

        idx = 0
        center_lis = []
        pos_lis = []
        neg_lis = []


        #使用负采样技术, 加大对低频词的注意力
        #正向采样时, 打压高频词
        iidx = 0
        center_word_lis = []

        while iidx < batch_size:
            word_idx = self.data[self.center_word_idx]

            # 若在词表中, 且保留
            if  word_idx != -1 and np.random.binomial(1, self.keeprate[word_idx]) == 1:
                center_word_lis.append(self.center_word_idx)
                iidx = iidx + 1
            self.center_word_idx_plus() 
            

        # 遍历所有的中心词, 找到其对应的词
        for k in range(len(center_word_lis)):
            pos_lis_for_c = self.get_corrsponding_pos_idx(center_word_lis[k], window_size)
            for i in range(len(pos_lis_for_c)):
                center_lis.append(self.data[ center_word_lis[k] ])
                pos_lis.append(self.data[ pos_lis_for_c[i] ])
                
            
        center_lis = np.array(center_lis)
        pos_lis = np.array(pos_lis)
        neg_lis = np.array(neg_lis)


        neg_lis = np.random.choice(a = self.idx_lis, size = (pos_lis.shape[0], neg_num), p = self.neg_frequency)

        return center_lis, pos_lis, neg_lis
            

    
    def get_corrsponding_pos_idx(self, pos_idx, window_size):
        #Try to find a negative inedx, if not return -1
        pos_lis = []
        possible_locations = np.array(list(range(max(0, pos_idx - window_size), min(pos_idx + window_size + 1, self.data_length - 1))))
        for k in possible_locations:
            if k != pos_idx and self.data[k] != -1  and np.random.binomial(1, self.keeprate[self.data[k]]) == 1:
                pos_lis.append(k)

        return pos_lis

        
    def center_word_idx_plus(self):
        if self.center_word_idx + 1 == self.data_length:
            self.epoches = self.epoches + 1
        self.center_word_idx = (self.center_word_idx + 1) % self.data_length



if __name__ == '__main__':
    data_loder = TrainDataLoader()
    center_lis, pos_lis, neg_lis = data_loder.get_batch()
    vocabulary_size = len(data_loder.word2idx.keys())
    print(vocabulary_size)
    print(center_lis.shape)
    print(pos_lis.shape)
    print(neg_lis.shape)

