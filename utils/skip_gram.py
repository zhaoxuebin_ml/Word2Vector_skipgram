#A torch implementation of skip gram
import numpy as np

import torch
from torch import Tensor
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
from typing import Type, Any, Callable, Union, List, Optional


class SkipGram(nn.Module):
    def __init__(self, vocabulary_size: int, dim: int = 128, SAVE_PATH = './data/') -> None: 
        super(SkipGram, self).__init__()

        #词向量是可学习的参数
        self.V = nn.Embedding(vocabulary_size, dim, sparse=True)   
        self.U = nn.Embedding(vocabulary_size, dim, sparse=True) 

        #初始化很重要  
        initrange = 0.5 / dim
        self.U.weight.data.uniform_(-initrange, initrange)
        self.V.weight.data.uniform_(-0, 0)

        self.SAVE_PATH_V = SAVE_PATH + 'V.npy'
        self.SAVE_PATH_U = SAVE_PATH + 'U.npy'



    def forward(self, center_word_lis, pos_lis, neg_lis):
        # torch.LongTensor 对应64位整数: long 而默认的tensor对应 32位浮点数float
        center_word_lis = Variable(torch.LongTensor(center_word_lis))
        pos_lis = Variable(torch.LongTensor(pos_lis))
        neg_lis = Variable(torch.LongTensor(neg_lis))
        
        if torch.cuda.is_available():
            center_word_lis = center_word_lis.cuda()
            pos_lis = pos_lis.cuda()
            neg_lis = neg_lis.cuda()


        embed_u = self.U(center_word_lis)
        embed_v = self.V(pos_lis)

        # 这两步相当于实现了u^T * v
        score  = torch.mul(embed_u, embed_v)
        score = torch.sum(score, dim=1)

        #Loss函数的前半部分算出来了
        log_target = F.logsigmoid(score)

        neg_embed_v = self.V(neg_lis)
        neg_score = torch.bmm(neg_embed_v, embed_u.unsqueeze(2)).squeeze()
        neg_score = torch.sum(neg_score, dim=1)
        sum_log_sampled = F.logsigmoid(-1*neg_score).squeeze()

        loss = log_target + sum_log_sampled
        return -1*loss.sum()/pos_lis.shape[0]

    # 保存数据
    def save_wordvector(self):
        U_saved = self.U.weight.data.cpu()
        V_saved = self.V.weight.data.cpu()
        np.save(self.SAVE_PATH_U, U_saved)
        np.save(self.SAVE_PATH_V, V_saved)
        print('word vector saved')
    


if __name__ == '__main__':
    sg = SkipGram(500)
    center_lis = np.array([1,2,3,4])
    pos_lis = np.array( [ 11, 21, 31, 32 ])
    neg_lis = np.array( [ [14,15,16,17,18,19], [14,15,16,17,18,19], [14,15,16,17,18,19], [14,15,16,17,18,19]]) 
    sg.forward(center_lis, pos_lis, neg_lis)
    #sg.save_wordvector()
