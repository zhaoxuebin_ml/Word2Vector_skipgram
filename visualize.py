from sklearn.manifold import TSNE
import numpy as np
import matplotlib.pyplot as plt
import random


VOCABULARY_PATH = './data/text8'
DATA_PATH = './data/data.npy'
WORD2IDX_PATH =  './data/word2idx.npy'
IDX2WORD_PATH =  './data/idx2word.npy'
FREQUENCY_PATH =   './data/frequency.npy'

word2idx = np.load(WORD2IDX_PATH, allow_pickle = True).item()
idx2word = np.load(IDX2WORD_PATH, allow_pickle = True).item()

U = np.load('./data/U.npy')
V = np.load('./data/V.npy')

# U = np.load('U.npy')
# V = np.load('V.npy')


word = word2idx.keys()
word = list(word)
random.shuffle(word)
word = word[:300]
vector = []
for w in word:
    v = U[word2idx[w]] + V[word2idx[w]]
    vector.append(v)
tsne = TSNE(n_components = 2, init = 'pca', verbose = 1)
embedd = tsne.fit_transform(vector)

plt.figure(figsize = (40,25))
plt.scatter(embedd[:,0], embedd[:,1])

for k in range(300):
    x = embedd[k,0]
    y = embedd[k,1]
    plt.text(x,y,word[k])
plt.savefig('vis.png')